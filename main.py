from flask import Flask, render_template, request, redirect, session, flash, url_for
from dao import NipDao
from models import Nip

from datetime import datetime
import re
import ldap
import ldap.filter
import subprocess


app = Flask(__name__)
app.secret_key = 'LlX$3h5GH69X'

pwd = "dbapssta368"
usr = "dbaps"
dsn = "10.200.0.211/PRDME"

nip_dao = NipDao(usr=usr,pwd=pwd,dsn=dsn)



@app.route('/')
def index():
    if 'usuario_logado' not in session or session['usuario_logado'] is None:
        return redirect(url_for('autenticarPorLdap', proxima=url_for('index')))
    nips = nip_dao.listar()
    return render_template('lista.html', nips=nips, titulo='Nips')

@app.route('/paginas')
def paginas():
    return render_template('pagination.html')

@app.route('/novo')
def novo():
    if 'usuario_logado' not in session or session['usuario_logado'] == None:
        return redirect(url_for('autenticarPorLdap', proxima=url_for('novo')))
    return render_template('novo.html', titulo='Nova NIP')


@app.route('/criar', methods=['POST', ])
def criar():

    ultima_modificacao = session['usuario_logado'] + ' - ' + str(datetime.now())
    mes = request.form['mes']
    data_notificacao = re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', request.form['data_notificacao'])
    demanda = request.form['demanda']
    beneficiario = request.form['beneficiario']
    cpf = request.form['cpf']
    produto = request.form['produto']
    cidade_origem = request.form['cidade_origem']
    plano = request.form['plano']
    vendedor = request.form['vendedor']
    motivo = request.form['motivo']
    observacao = request.form['observacao']
    natureza = request.form['natureza']
    status = request.form['status']
    data_classificacao = re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', request.form['data_classificacao'])
    tipo_contrato = request.form['tipo_contrato']
    protocolo = request.form['protocolo']
    data_adesao = re.sub(r'(\d{4})-(\d{1,2})-(\d{1,2})', '\\3-\\2-\\1', request.form['data_adesao'])
    empresa = request.form['empresa']
    submotivo2 = request.form['submotivo2']
    classificacao = request.form['classificacao']
    area_envolvida1 = request.form['area_envolvida1']
    area_envolvida2 = request.form['area_envolvida2']
    monitoramento = request.form['monitoramento']

    nip = Nip(mes,
              data_notificacao,
              demanda,
              beneficiario,
              cpf,
              produto,
              cidade_origem,
              plano,
              vendedor,
              motivo,
              observacao,
              natureza,
              status,
              data_classificacao,
              tipo_contrato,
              protocolo,
              data_adesao,
              empresa,
              submotivo2,
              classificacao,
              area_envolvida1,
              area_envolvida2,
              monitoramento,
              ultima_modificacao
              )
    nip_dao.salvar(nip)
    print(session['usuario_logado'] + ' - /criar')
    return redirect(url_for('index'))


@app.route('/editar/<int:id>')
def editar(id):
    if 'usuario_logado' not in session or session['usuario_logado'] == None:
        return redirect(url_for('login', proxima=url_for('editar')))

    nip = nip_dao.busca_por_id(id)

    return render_template('editar.html', titulo='Editando NIP', nip=nip)

@app.route('/atualizar', methods=['POST', ])
def atualizar():

    ultima_modificacao = session['usuario_logado'] + ' - ' + str(datetime.now())
    mes = request.form['mes']
    data_notificacao = request.form['data_notificacao']
    demanda = request.form['demanda']
    beneficiario = request.form['beneficiario']
    cpf = request.form['cpf']
    produto = request.form['produto']
    cidade_origem = request.form['cidade_origem']
    plano = request.form['plano']
    vendedor = request.form['vendedor']
    motivo = request.form['motivo']
    observacao = request.form['observacao']
    natureza = request.form['natureza']
    status = request.form['status']
    data_classificacao = request.form['data_classificacao']
    tipo_contrato = request.form['tipo_contrato']
    protocolo = request.form['protocolo']
    data_adesao = request.form['data_adesao']
    empresa = request.form['empresa']
    submotivo2 = request.form['submotivo2']
    classificacao = request.form['classificacao']
    area_envolvida1 = request.form['area_envolvida1']
    area_envolvida2 = request.form['area_envolvida2']
    monitoramento = request.form['monitoramento']
    id = request.form['id_registro']

    nip = Nip(mes,
              data_notificacao,
              demanda,
              beneficiario,
              cpf,
              produto,
              cidade_origem,
              plano,
              vendedor,
              motivo,
              observacao,
              natureza,
              status,
              data_classificacao,
              tipo_contrato,
              protocolo,
              data_adesao,
              empresa,
              submotivo2,
              classificacao,
              area_envolvida1,
              area_envolvida2,
              monitoramento,
              ultima_modificacao,
              id=id
              )

    nip_dao.salvar(nip)
    print(session['usuario_logado'] + ' - /atualizar')
    return redirect(url_for('index'))

@app.route('/deletar/<int:id>')
def deletar(id):

    nip_dao.deletar(id)
    flash('Nip removida com sucesso!', 'alert-success')
    print(session['usuario_logado'] + ' - /deletar/<int:id>')
    return redirect(url_for('index'))


@app.route('/login')
def login():
    proxima = request.args.get('proxima')
    return render_template('login.html', proxima=proxima)


@app.route('/logout')
def logout():
    session['usuario_logado'] = None
    flash('Usuário deslogado!', 'alert-success')
    print('/logout - session = ', session['usuario_logado'])
    return redirect(url_for('login'))

@app.route('/autenticacaomanual', methods=['post', ])
def autenticacaoManual():

    con = ldap.initialize("ldap://10.201.0.250:389")
    user_dn = request.form['usuario'] + "@ascs.intra"
    password = request.form['senha']

    list_usuario = ['pmpureza', 'cobarrinha','gaafbarbosa','bmalves','alpsilva','tvlsilva']
    if request.form['usuario'] in list_usuario:
        try:
            con.simple_bind_s(user_dn, password)
            flash(request.form['usuario'] + ' Logou com sucesso!', 'alert-success')
            session['usuario_logado'] = request.form['usuario']
            proxima_pagina = request.form['proxima']
            return redirect(proxima_pagina)
        except Exception as e:
            flash('Usuario incorreto, tente novamente!', 'alert-danger')
            return redirect(url_for('login'))
        else:
            flash('Usuario sem acesso!', 'alert-danger')
            return redirect(url_for('login'))

@app.route('/autenticaldap')
def autenticarPorLdap():

    ip_client = request.remote_addr
    batcmd = "wmic / node: {} computersystem get username".format(ip_client)
    usuario_pc = str(subprocess.check_output(batcmd, shell=True)).split("\\")[5].strip()

    list_usuario = ['pmpureza', 'cobarrinha', 'gaafbarbosa','bmalves','alpsilva','tvlsilva']
    if usuario_pc in list_usuario:

        con = ldap.initialize("ldap://10.201.0.250:389")
        user_dn = r"mvaut@ascs.intra"
        password = "Mudar@123"
        criteria = ldap.filter.filter_format('(&(objectClass=user)(sAMAccountName=%s))', ['{}'.format(usuario_pc)])

        try:
            con.simple_bind_s(user_dn, password)
            res = con.search_s("OU=ASSOCIACAO,DC=ascs,DC=intra", ldap.SCOPE_SUBTREE, criteria)
            if len(res) > 0:
                session['usuario_logado'] = usuario_pc
                flash(usuario_pc + ' Logou com sucesso!', 'alert-success')
                proxima_pagina = request.args.get('proxima')
                print(session['usuario_logado'] + ' - /autenticarPorLdap')
                return redirect(proxima_pagina)
            else:
                flash('Usuário não encontrado, faça Login manualmente!', 'alert-danger')
                return redirect(url_for('login', proxima=url_for('index')))
        except Exception as error:
            usuario_logado = error
            print(error)
        return redirect(url_for('login', proxima=url_for('index')))
    else:
        flash('Usuário sem acesso!', 'alert-danger')
        return redirect(url_for('login', proxima=url_for('index')))


if __name__ == "__main__":
    # app.run(debug=True, host='0.0.0.0',threaded=True)
    from waitress import serve
    serve(app, host='0.0.0.0', port=5000)