class Nip():

    def __init__(self,
                 mes,
                 data_notificacao,
                 demanda,
                 beneficiario,
                 cpf,
                 produto,
                 cidade_origem,
                 plano,
                 vendedor,
                 motivo,
                 observacao,
                 natureza,
                 status,
                 data_classificacao,
                 tipo_contrato,
                 protocolo,
                 data_adesao,
                 empresa,
                 submotivo2,
                 classificacao,
                 area_envolvida1,
                 area_envolvida2,
                 monitoramento,
                 ultima_modificacao,
                 id=None,
                 ):
        self.mes = mes
        self.data_notificacao = data_notificacao
        self.demanda = demanda
        self.beneficiario = beneficiario
        self.cpf = cpf
        self.produto = produto
        self.cidade_origem = cidade_origem
        self.plano = plano
        self.vendedor = vendedor
        self.motivo = motivo
        self.observacao = observacao
        self.natureza = natureza
        self.status = status
        self.data_classificacao = data_classificacao
        self.tipo_contrato = tipo_contrato
        self.protocolo = protocolo
        self.data_adesao = data_adesao
        self.empresa = empresa
        self.submotivo2 = submotivo2
        self.classificacao = classificacao
        self.area_envolvida1 = area_envolvida1
        self.area_envolvida2 = area_envolvida2
        self.monitoramento = monitoramento
        self.ultima_modificacao = ultima_modificacao
        self.id = id

class Usuario:
    def __init__(self, id, nome, senha):
        self.id = id
        self.nome = nome
        self.senha = senha






