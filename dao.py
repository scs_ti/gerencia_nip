from models import Usuario,Nip
import cx_Oracle

SQL_DELETA_NIP = 'delete from NIPS_GERENCIAIS  where id = {}'

SQL_NIP_POR_ID = '''SELECT 
                        mes,
                        data_notificacao,
                        demanda,
                        beneficiario,
                        cpf,
                        produto,
                        cidade_origem,
                        plano,
                        vendedor,
                        motivo,
                        observacao,
                        natureza,
                        status,
                        data_classificacao,
                        tipo_contrato,
                        protocolo,
                        data_adesao,
                        empresa,
                        submotivo2,
                        classificacao,
                        area_envolvida1,
                        area_envolvida2,
                        monitoramento,
                        ultima_modificacao,
                        id
                        
from NIPS_GERENCIAIS where id = {}'''

SQL_USUARIO_POR_ID = '''SELECT id, nome, senha from USUARIO_GERENCIA_NIPS where id = '{}' '''

SQL_ATUALIZA_NIP = '''
UPDATE NIPS_GERENCIAIS
 SET 
    mes = '{}',
    data_notificacao = '{}',
    demanda = '{}',
    beneficiario = '{}',
    cpf = '{}',
    produto = '{}',
    cidade_origem = '{}',
    plano = '{}',
    vendedor = '{}',
    motivo = '{}',
    observacao = '{}',
    natureza = '{}',
    status = '{}',
    data_classificacao = '{}',
    tipo_contrato = '{}',
    protocolo = '{}',
    data_adesao = '{}',
    empresa = '{}',
    submotivo2 = '{}',
    classificacao = '{}',
    area_envolvida1 = '{}',
    area_envolvida2 = '{}',
    monitoramento = '{}',
    ultima_modificacao = '{}'
 where id = '{}' '''

SQL_BUSCA_NIPS = ''' SELECT  
                 mes,
                 data_notificacao,
                 demanda,
                 beneficiario,
                 cpf,
                 produto,
                 cidade_origem,
                 plano,
                 vendedor,
                 motivo,
                 observacao,
                 natureza,
                 status,
                 data_classificacao,
                 tipo_contrato,
                 protocolo,
                 data_adesao,
                 empresa,
                 submotivo2,
                 classificacao,
                 area_envolvida1,
                 area_envolvida2,
                 monitoramento,
                 ultima_modificacao,
                 id
                  from NIPS_GERENCIAIS'''

SQL_CRIA_NIP = '''
INSERT into NIPS_GERENCIAIS (
                    mes,
                    data_notificacao,
                    demanda,
                    beneficiario,
                    cpf,
                    produto,
                    cidade_origem,
                    plano,
                    vendedor,
                    motivo,
                    observacao,
                    natureza,
                    status,
                    data_classificacao,
                    tipo_contrato,
                    protocolo,
                    data_adesao,
                    empresa,
                    submotivo2,
                    classificacao,
                    area_envolvida1,
                    area_envolvida2,
                    monitoramento,
                    ultima_modificacao
                      )
                       values ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')'''


class NipDao:
    def __init__(self, usr,pwd, dsn):
        self.__usr = usr
        self.__pwd = pwd
        self.__dsn = dsn

    def salvar(self, nip):
        with cx_Oracle.connect(user=self.__usr, password=self.__pwd, dsn=self.__dsn, encoding="UTF-8") as connection:
            cursor = connection.cursor()
            if (nip.id):
                cursor.execute(SQL_ATUALIZA_NIP.format(
                    nip.mes,
                    nip.data_notificacao,
                    nip.demanda,
                    nip.beneficiario,
                    nip.cpf,
                    nip.produto,
                    nip.cidade_origem,
                    nip.plano,
                    nip.vendedor,
                    nip.motivo,
                    nip.observacao,
                    nip.natureza,
                    nip.status,
                    nip.data_classificacao,
                    nip.tipo_contrato,
                    nip.protocolo,
                    nip.data_adesao,
                    nip.empresa,
                    nip.submotivo2,
                    nip.classificacao,
                    nip.area_envolvida1,
                    nip.area_envolvida2,
                    nip.monitoramento,
                    nip.ultima_modificacao,
                    nip.id
                )
                               )
            else:
                cursor.execute(SQL_CRIA_NIP.format(
                    nip.mes,
                    nip.data_notificacao,
                    nip.demanda,
                    nip.beneficiario,
                    nip.cpf,
                    nip.produto,
                    nip.cidade_origem,
                    nip.plano,
                    nip.vendedor,
                    nip.motivo,
                    nip.observacao,
                    nip.natureza,
                    nip.status,
                    nip.data_classificacao,
                    nip.tipo_contrato,
                    nip.protocolo,
                    nip.data_adesao,
                    nip.empresa,
                    nip.submotivo2,
                    nip.classificacao,
                    nip.area_envolvida1,
                    nip.area_envolvida2,
                    nip.monitoramento,
                    nip.ultima_modificacao
                )
                               )
                nip.id = cursor.lastrowid
            connection.commit()
        return nip

    def listar(self):
        with cx_Oracle.connect(user=self.__usr, password=self.__pwd, dsn=self.__dsn, encoding="UTF-8") as connection:
            cursor = connection.cursor()
            cursor.execute(SQL_BUSCA_NIPS)
            nips = traduz_nips(cursor.fetchall())
        return nips

    def busca_por_id(self, id):
        with cx_Oracle.connect(user=self.__usr, password=self.__pwd, dsn=self.__dsn, encoding="UTF-8") as connection:
            cursor = connection.cursor()
            cursor.execute(SQL_NIP_POR_ID.format(id))
            tupla = cursor.fetchone()
        return Nip(tupla[0],
                   tupla[1],
                   tupla[2],
                   tupla[3],
                   tupla[4],
                   tupla[5],
                   tupla[6],
                   tupla[7],
                   tupla[8],
                   tupla[9],
                   tupla[10],
                   tupla[11],
                   tupla[12],
                   tupla[13],
                   tupla[14],
                   tupla[15],
                   tupla[16],
                   tupla[17],
                   tupla[18],
                   tupla[19],
                   tupla[20],
                   tupla[21],
                   tupla[22],
                   tupla[23],
                   tupla[24],
                   )

    def deletar(self, id):
        with cx_Oracle.connect(user=self.__usr, password=self.__pwd, dsn=self.__dsn, encoding="UTF-8") as connection:
            cursor = connection.cursor()
            cursor().execute(SQL_DELETA_NIP.format(id))
            connection.commit()


def traduz_nips(nips):
    def cria_nip_com_tupla(tupla):
        return Nip(tupla[0],
                   tupla[1],
                   tupla[2],
                   tupla[3],
                   tupla[4],
                   tupla[5],
                   tupla[6],
                   tupla[7],
                   tupla[8],
                   tupla[9],
                   tupla[10],
                   tupla[11],
                   tupla[12],
                   tupla[13],
                   tupla[14],
                   tupla[15],
                   tupla[16],
                   tupla[17],
                   tupla[18],
                   tupla[19],
                   tupla[20],
                   tupla[21],
                   tupla[22],
                   tupla[23],
                   id=tupla[24])
    return list(map(cria_nip_com_tupla, nips))


def traduz_usuario(tupla):
    return Usuario(tupla[0], tupla[1], tupla[2])
